//
//  KGRandomNewPhotosRequest.m
//  DebuggingAppV2
//
//  Created by Kendall Helmstetter Gelner on 9/7/12.
//  Copyright (c) 2012 KiGi Software, LLC. All rights reserved.
//

#import "KGRandomNewPhotosRequest.h"
#import "KGFlickrItem.h"
#import "KGFlickrMedia.h"
#import "KGFlickrTag.h"
#import "NSDictionary+KGSafeExtract.h"

@implementation KGRandomNewPhotosRequest


+ (void) performRequestWithSuccessHandler:(void (^)(NSArray *flickrItems))success
                           failureHandler:(void (^)(NSError *error))failure
                           finallyHandler:(void (^)())finally
{
    NSString *allPhotosPath = @"/services/feeds/photos_public.gne";
    NSDictionary *jsonFormatParam = @{@"format":@"json",
                                      @"nojsoncallback":@"1"};
    
    [self getPath:allPhotosPath parameters:jsonFormatParam successHandler:^(NSURLResponse *response, id responseObject) {
        // Parse JSON from server
        if ( [responseObject isKindOfClass:[NSData class]] )
        {
            NSData *responseData =responseObject;
            // In real life, AFNetworking would already have decoded the JSON.  Some bug in AFNetwork prevents parsing correctly, so manually parse if needed.
            responseObject = [self tryMassagedData:responseData];
        }
        
        if ( [responseObject isKindOfClass:[NSDictionary class]] )
        {
            NSDictionary *flickrMetaDict = responseObject;
            [self convertJSONToObjects:flickrMetaDict];
            if ( success )
                success( nil );
        }
    } failureHandler:^(NSURLResponse *response, NSError *error) {
        if ( failure )
        {
            failure(error);
        }
    }
   finallyHandler:finally
     ];
}

@end
