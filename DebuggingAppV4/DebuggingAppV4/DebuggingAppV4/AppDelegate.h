//
//  AppDelegate.h
//  DebuggingAppV4
//
//  Created by Kendall Helmstetter Gelner on 8/16/14.
//  Copyright (c) 2014 KiGi Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

