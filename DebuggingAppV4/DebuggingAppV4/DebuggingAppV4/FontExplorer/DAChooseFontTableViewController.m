//
//  DAChooseFontTableViewController.m
//  DebuggingApp
//
//  Created by Kendall Gelner on 9/24/09.
//  Copyright 2009 KiGi Software. All rights reserved.
//

#import "DAChooseFontTableViewController.h"

#import "DAMainFontExplorer.h"
#import "DAPossibleFonts.h"


@interface DAChooseFontTableViewController()
@property (nonatomic, strong) void (^refetcher)(void);

@property (nonatomic, strong) NSArray *fontList;
@property (nonatomic, strong)  IBOutlet UITableView *myTable;

@property (nonatomic, assign) UIView *crashView;

@end

@implementation DAChooseFontTableViewController

@synthesize delegate = delegate_;
@synthesize refetcher = refetcher_;


- (void) refreshFontList
{
    self.fontList = [[DAPossibleFonts sharedInstance] fullFontList];
    [self.myTable reloadData];
}

- (void)viewDidLoad 
{	
	self.title = @"Choose Font";
    
    [self refreshFontList];
        
    __block __typeof__(self) bself = self;
    self.refetcher = ^{
        [bself refreshFontList];
    };
	
    // Uncomment for crash!
    //self.crashView = [[UIView alloc] init];
    
	//[self performSelector:@selector(hurryUp) withObject:nil afterDelay:7.0f];
    [self delayedCall];
	[super viewDidLoad];
}

- (void) delayedCall
{
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 15.0f * NSEC_PER_SEC);
    
    dispatch_after(popTime, dispatch_get_main_queue(), ^{
        [self hurryUp];
    });
}


- (void) hurryUp
{
	NSLog(@"Hurry!");
}

- (void)viewWillAppear:(BOOL)animated {
//    DAVCTracker *useTracker = [DATrackingManager trackingForVC:[self class]];
//    useTracker.VCLengthAccessed = 0;
//    self.tracker = useTracker;
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    //self.tracker.VCLengthAccessed = 30.0;
    //[DATrackingManager saveVCTrackingResults];
	[super viewWillDisappear:animated];
}


#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.fontList.count;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
	BOOL createdCell;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
		createdCell = YES;
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Set up the cell...
    NSInteger fontIndex = 5;
	NSString *fontName = [self.fontList objectAtIndex:fontIndex];
	cell.textLabel.font = [UIFont fontWithName:fontName size:17.0f];
	cell.textLabel.text = fontName;
	
	if ( [self.delegate.chosenFont isEqualToString:fontName] )
		cell.accessoryType = UITableViewCellAccessoryCheckmark;
	else
		cell.accessoryType = UITableViewCellAccessoryNone;
    
    if ( indexPath.row == 20 ) {
        //NSLog(@"CrashView: %@", self.crashView);
        //[self.crashView performSelector:@selector(backgroundColor) withObject:nil afterDelay:1.0f];
        //		NSLog(@"Created Cell %d", cell);
    }
	
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
	self.delegate.chosenFont = [self.fontList objectAtIndex:indexPath.row];
	[self.navigationController popViewControllerAnimated:YES];
}




@end


