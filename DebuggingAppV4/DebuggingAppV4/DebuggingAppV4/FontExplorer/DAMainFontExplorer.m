//
//  DAMainFontExplorer.m
//  DebuggingApp
//
//  Created by Kendall Gelner on 9/23/09.
//  Copyright 2009 KiGi Software. All rights reserved.
//

#import "DAMainFontExplorer.h"
#import "DAChooseFontTableViewController.h"
#import "DAPossibleFonts.h"

@interface DAMainFontExplorer ()
@property (nonatomic) NSInteger selectedCell;
@property (nonatomic, retain) NSTimer *timer;
@property (nonatomic, strong) UIBarButtonItem *chooseFontButton;

@property (nonatomic, weak) IBOutlet UITableView *myTable;
@property (nonatomic, weak) IBOutlet UITextField *customTextField;
@property (nonatomic, weak) IBOutlet UIButton *revButton;

@end


@implementation DAMainFontExplorer


#pragma mark -
#pragma mark TextField methods

#define KBD_HEIGHT 150

- (IBAction) keyboardWillAppear:(NSNotification *)notification
{
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(doneEditing)];
}

- (void) doneEditing
{
	[self.customTextField resignFirstResponder];
    self.navigationItem.rightBarButtonItem = self.chooseFontButton;
}

- (IBAction) keyboardWillDisappear:(NSNotification *)notification
{
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[self doneEditing];
	return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	// reloads string after new string is set.
    NSString * proposedNewString = [[textField text] stringByReplacingCharactersInRange:range withString:string];
    [self reloadExampleString:proposedNewString];
	return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    [self reloadExampleString:@""];
	return YES;
}

- (void) reloadExampleString:(NSString *)newString
{
	if ( newString.length == 0 )
	{
		self.fontExampleString = @"Example Text";
	}
	else
	{
		self.fontExampleString = newString;
	}

	[self.myTable reloadData];
}

#pragma mark -
#pragma mark Helper methods
//- (CGFloat)determineLabelHeightForIndexPath:(NSIndexPath *)indexPath
//{
//	// You have to use some kind of maximum, so we'll use an unlikley max.
//	int maxHeight = 99999;
//	
//	
//	CGSize maxSize = CGSizeMake(exampleCell.exampleTextLabel.frame.size.width, maxHeight);
//    if ( maxSize.width < 305 || maxSize.width > 320 )
//        maxSize.width = 320;
//	int row = indexPath.row;
//	UIFont *font = [UIFont fontWithName:self.chosenFont size:row];
//	// This call is theoretically better, but does not actually work (always returns one row).  
//	//CGSize bestSize = [fontExampleString sizeWithFont:font  forWidth:maxSize.width lineBreakMode:UILineBreakModeWordWrap];
//	CGSize bestSize = [fontExampleString sizeWithFont:font constrainedToSize:maxSize lineBreakMode:NSLineBreakByWordWrapping];
//	
//	return bestSize.height;
//}

#pragma mark -
#pragma mark Actions


- (IBAction) reversePressed:(id)sender
{
    __block __typeof(self) __weak welf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *revString = welf.customTextField.text;
        NSInteger len = revString.length;
        NSMutableString *reversedString = [NSMutableString stringWithCapacity:len];
        
        while (len > 0)
        {
            unichar uch = [revString characterAtIndex:--len];
            [reversedString appendString:[NSString stringWithCharacters:&uch length:1]];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            welf.customTextField.text = reversedString;
            [welf reloadExampleString:reversedString];
        });
    });
}

- (void)timerCheckpoint:(NSTimer*)theTimer
{
    NSLog(@"Hit timer checkpoint, textfield contents = %@", self.customTextField.text);
}

#pragma mark -
#pragma mark UIViewController Methods


- (void)viewDidLoad 
{
	if ( self.chosenFont == nil )
		self.chosenFont = [[[DAPossibleFonts sharedInstance] fullFontList] objectAtIndex:0];

    self.chooseFontButton = self.navigationItem.rightBarButtonItem;
    
	self.timer = [NSTimer scheduledTimerWithTimeInterval:10.0f target:self selector:@selector(timerCheckpoint:) userInfo:nil repeats:YES];
    [self.timer fire];
    
    
//    
//    NSRunLoop * theRunLoop = [NSRunLoop currentRunLoop];
//    [theRunLoop addTimer:self.timer forMode:NSDefaultRunLoopMode];
    
	// initializes example string to default value
    [self reloadExampleString:@""];
	
	// Add back button here for use by subview we push to
	UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle: @"Cancel" style:UIBarButtonItemStylePlain target:nil action:nil];
	self.navigationItem.backBarButtonItem = backButton;
	
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated 
{
    [super viewWillAppear:animated];
    
    self.navigationItem.title = self.chosenFont;
    [self.myTable reloadData];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardWillAppear:) 
												 name:UIKeyboardWillShowNotification
											   object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardWillDisappear:) 
												 name:UIKeyboardWillHideNotification
											   object:nil];	
    
}

- (void) viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
	[super viewWillDisappear:animated];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [segue.destinationViewController isKindOfClass:[DAChooseFontTableViewController class]] )
    {
        DAChooseFontTableViewController *chooser = (DAChooseFontTableViewController *)segue.destinationViewController;
        chooser.delegate = self;
    }
}


#pragma mark -
#pragma mark Table view methods

#define MAX_FONT_SIZE 100.0
#define MIN_FONT_SIZE 6.0

- (NSInteger) calculateRowHeightFromRow:(NSInteger)rowNum
{
    // Could calculate exactly by measuring lable height, but
    // we'll cheat and just use a pretty linear progression of increase
    return 40.0 + (((float)rowNum + MIN_FONT_SIZE) * 1.2);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return MAX_FONT_SIZE;
}

// Uncomment to control the height of cells in the table
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height = [self calculateRowHeightFromRow:indexPath.row];
	return height;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"FontSizeCell";
    
    FontSizeCell *cell = (FontSizeCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Set up the cell...
    
    NSString *fontName = self.chosenFont;
    
	[cell setFontName:fontName size:indexPath.row + MIN_FONT_SIZE exampleText:self.fontExampleString];
	
	if ( indexPath.row == self.selectedCell )
		cell.accessoryType = UITableViewCellAccessoryCheckmark;
	else
		cell.accessoryType = UITableViewCellAccessoryNone;

	
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
   
	NSString *rowDescription;
	NSIndexPath *oldIndexPath = [NSIndexPath indexPathForRow:self.selectedCell inSection:0];
	
	self.selectedCell = indexPath.row;
	
	// Clear old cell
	[tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:oldIndexPath] withRowAnimation:UITableViewRowAnimationFade];
	
	// Check new cell
	[tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];

	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	if ( indexPath.row == 5 )
	{
		NSLog(@"Row description:%@", rowDescription);
	}
	
}



@end





