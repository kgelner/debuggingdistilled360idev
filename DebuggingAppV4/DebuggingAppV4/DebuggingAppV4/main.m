//
//  main.m
//  DebuggingAppV4
//
//  Created by Kendall Helmstetter Gelner on 8/16/14.
//  Copyright (c) 2014 KiGi Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
