//
//  ViewController.swift
//  Use as template code for new view controllers in Swift
//
//  Created by Kendall Helmstetter Gelner on 7/27/14.
//  Copyright (c) 2014 KiGi, LLC. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
                            
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

}

