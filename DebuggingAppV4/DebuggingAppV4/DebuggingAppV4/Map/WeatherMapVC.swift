//
//  WeatherMap.swift
//  DebuggingAppV4
//
//  Created by Kendall Helmstetter Gelner on 8/22/14.
//  Copyright (c) 2014 KiGi Software, LLC. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import MapKit

class WeatherMapVC: UIViewController, CLLocationManagerDelegate {
    
    // Class Var
    var locationManager : CLLocationManager!
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var daySegment: UISegmentedControl!
    
    @IBAction func daySegmentChanged(sender: AnyObject) {
    }
    
    // View Controller
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        locationManager = CLLocationManager()
        setupLocationManager()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func setupLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        if locationManager.respondsToSelector(Selector("requestAlwaysAuthorization")) {
            locationManager.requestWhenInUseAuthorization()
        }

        locationManager.startUpdatingLocation()

    }
    
    // Location Delegate
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!)
    {
        if locations != nil && locations.count > 0 {
            let firstLocation : CLLocation = locations.first as CLLocation
            println("New locations : \(locations)")
        }
    }
    
    func locationManager(manager: CLLocationManager!,
        didChangeAuthorizationStatus status: CLAuthorizationStatus)
    {
        switch status {
        case .Authorized, .AuthorizedWhenInUse:
            println("Allowed: \(status)")
            mapView.showsUserLocation = true
        case .Denied:
                locationManager.stopUpdatingLocation()
        default:
            println("State is \(status)")
        }
    }
    
}