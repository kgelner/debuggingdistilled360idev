//
//  DebuggingAppV4Tests.m
//  DebuggingAppV4Tests
//
//  Created by Kendall Helmstetter Gelner on 8/16/14.
//  Copyright (c) 2014 KiGi Software, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>

@interface DebuggingAppV4Tests : XCTestCase

@end

@implementation DebuggingAppV4Tests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    XCTAssert(YES, @"Pass");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
}

@end
