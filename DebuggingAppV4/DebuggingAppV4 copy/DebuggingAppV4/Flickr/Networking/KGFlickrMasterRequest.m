//
//  KGMasterRequest.m
//  DebuggingAppV2
//
//  Created by Kendall Helmstetter Gelner on 9/7/12.
//  Copyright (c) 2012 KiGi Software, LLC. All rights reserved.
//

#import "KGFlickrMasterRequest.h"

#import "KGFlickrItem.h"
#import "KGFlickrMedia.h"
#import "KGFlickrTag.h"
#import "NSDictionary+KGSafeExtract.h"


@implementation KGFlickrMasterRequest

+ (void)getPath:(NSString *)path
     parameters:(NSDictionary *)parameters
 successHandler:(void (^)(NSURLResponse *response, id responseObject))success
 failureHandler:(void (^)(NSURLResponse *response, NSError *error))failure
 finallyHandler:(void (^)())finally
{
    NSString *fullPath = [NSString stringWithFormat:@"http://api.flickr.com%@", path];
    NSMutableString *baseString = [NSMutableString stringWithString:fullPath];
    if ( parameters.count > 0 ) {
        [baseString appendString:@"?"];
        [parameters enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            [baseString appendFormat:@"%@=%@&",
             [key stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],
             [obj stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        }];
        NSRange endRange = {baseString.length-1, 1};
        [baseString deleteCharactersInRange:endRange];
    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:baseString] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:20.0f];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if ( connectionError == nil ) {
            success( response, data );
        }
        else {
            if ( failure ) {
                failure( response, connectionError );
            }
        }
        
        if ( finally )
            finally();
    }];
}

+ (NSDate *)dateFromServerDate:(NSString *)serverDate
{
    __strong static NSDateFormatter *serverDateFormatter = nil;
    
    NSRange endRange = { serverDate.length - 4, 4 };
    NSString *convertServerDate = [serverDate stringByReplacingOccurrencesOfString:@":" withString:@"" options:NSBackwardsSearch range:endRange];
    
    if ( serverDateFormatter == nil )
    {
        serverDateFormatter = [[NSDateFormatter alloc] init];
        serverDateFormatter.dateFormat = [NSDateFormatter dateFormatFromTemplate:@"yyyy-MM-dd'T'HH:mm:ssZZZ" options:0 locale:[NSLocale currentLocale]];
    }
    
    NSDate *convertDate = nil;
    NSRange range = {0,0};
    NSError *error = nil;
    
    [serverDateFormatter getObjectValue:&convertDate forString:convertServerDate range:&range error:&error];
    
    return convertDate;
}


+ (void) convertJSONToObjects:(NSDictionary *)flickrMetaDict
{
    NSArray *items = [flickrMetaDict objectForKey:@"items"];
    
    for ( NSDictionary *itemDict in items )
    {
        KGFlickrItem *item = [KGFlickrItem createFlickrItem];
        
        item.title = [itemDict safeStringForKey:@"title"];
        item.link = [itemDict safeStringForKey:@"link"];
        NSString *rawDateTaken = [itemDict safeStringForKey:@"date_taken"];
        item.dateTaken = [self dateFromServerDate:rawDateTaken];
        item.flickrDescription = [itemDict safeStringForKey:@"description"];
        NSString *rawPublishDate = [itemDict safeStringForKey:@"published"];
        item.published = [self dateFromServerDate:rawPublishDate];
        item.authorName = [itemDict safeStringForKey:@"author"];
        item.authorFlickrID = [itemDict safeStringForKey:@"author_id"];
        
        // Pull out media items, usually just one
        NSDictionary *mediaDict = [itemDict objectForKey:@"media"];
        
        for ( NSString *key in [mediaDict allKeys] )
        {
            KGFlickrMedia *media = [KGFlickrMedia createFlickrMedia];
            NSString *urlString = [mediaDict safeStringForKey:key];
            media.mediaSize = key;
            media.mediaURL = urlString;
            media.item = item;
        }
        
        NSString *tags = [itemDict safeStringForKey:@"tags"];
        
        NSArray *tagsSeperated = [tags componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        for ( NSString *tagRaw in tagsSeperated )
        {
            KGFlickrTag *tag =[KGFlickrTag createFlickrTag];
            tag.tagValue = tagRaw;
            tag.complexTagValue = ([tagRaw rangeOfString:@":"].location != NSNotFound);
            tag.item = item;
        }
        //        LoggingOnlyComposeString debugString = ^ { return [NSString stringWithFormat:@"Original JSON:%@\nObject is %@", itemDict, item];};
        //DNSLog(debugString());
    }
    
    [KGFlickrItem saveAllModfiedFlickrItems];
    
}


+ (NSDictionary *) tryMassagedData:(NSData *)brokenData
{
    NSMutableString *jsonString = [[NSMutableString alloc] initWithData:brokenData encoding:NSUTF8StringEncoding];
    
    NSRange replaceRange = {0, jsonString.length};
    [jsonString replaceOccurrencesOfString:@"\\'" withString:@"'" options:0 range:replaceRange];
    NSData *fixedData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error = nil;
    NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:fixedData options:0 error:&error];
    
    if ( error )
        return nil;
    else
        return responseDict;
}

@end
