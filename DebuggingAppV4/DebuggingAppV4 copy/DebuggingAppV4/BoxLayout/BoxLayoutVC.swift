//
//  BoxLayoutVC.swift
//  DebuggingAppV4
//
//  Created by Kendall Helmstetter Gelner on 8/22/14.
//  Copyright (c) 2014 KiGi Software, LLC. All rights reserved.
//

import Foundation


import UIKit

class BoxLayoutVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView!) -> Int
    {
        return 1;
    }
    
    func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int
    {
        return 10;
    }

    func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell!
    {
        let cell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier("NADACELL") as UITableViewCell
        
        return cell
    }
    
}


