//
//  DAPossibleFonts.m
//  DebuggingApp
//
//  Created by Kendall Gelner on 9/23/09.
//  Copyright 2009 KiGi Software. All rights reserved.
//

#import "DAPossibleFonts.h"
#import <UIKit/UIKit.h>

static DAPossibleFonts *sharedInstance;

@implementation DAPossibleFonts

#pragma mark -
#pragma mark class instance methods

- (NSArray *)fullFontList
{
	allFonts = [[NSMutableArray alloc] init];
	
	NSArray *familyNames = [UIFont familyNames];
	NSArray *fontNames;
	NSInteger indFamily, indFont;
	for (indFamily=0; indFamily<[familyNames count]; ++indFamily)
	{
		NSLog(@"Family name: %@", [familyNames objectAtIndex:indFamily]);
		fontNames = [UIFont fontNamesForFamilyName:
					  [familyNames objectAtIndex:indFamily]];
		for (indFont=0; indFont<[fontNames count]; ++indFont)
		{
			NSLog(@"    Font name: %@ family:%ld", [fontNames objectAtIndex:indFont], (long)indFamily);
			[allFonts addObject:[fontNames objectAtIndex:indFont]];
		}
	}
	
	return allFonts;
}

#pragma mark -
#pragma mark Singleton methods

+ (DAPossibleFonts*)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[DAPossibleFonts alloc] init];
    });
    return sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone {
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [super allocWithZone:zone];
            return sharedInstance;  // assignment and return on first allocation
        }
    }
    return nil; // on subsequent allocation attempts return nil
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}


@end
