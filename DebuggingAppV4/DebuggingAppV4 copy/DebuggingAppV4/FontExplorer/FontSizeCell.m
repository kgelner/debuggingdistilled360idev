//
//  FontSizeCell.m
//  DebuggingApp
//
//  Created by Kendall Gelner on 9/24/09.
//  Copyright 2009 KiGi Software. All rights reserved.
//

#import "FontSizeCell.h"

@interface FontSizeCell()
@property (nonatomic, copy) NSString *sizeFormatString;
@property (nonatomic, copy) NSString *savedSize;
@property (nonatomic, weak) IBOutlet UILabel *sizeLabel;
@property (nonatomic, weak) IBOutlet UILabel *exampleTextLabel;

@end

@implementation FontSizeCell



- (void) awakeFromNib
{
	// Save off cell formatting string
    self.sizeFormatString = self.sizeLabel.text;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setFontName:(NSString *)fontName size:(CGFloat)size exampleText:(NSString *)exampleText
{
	self.exampleTextLabel.font = [UIFont fontWithName:fontName size:size];
    self.exampleTextLabel.text = exampleText;
    self.sizeLabel.text = [NSString stringWithFormat:self.sizeFormatString,(int)size];
	
//	if ( [fontName isEqualToString:@"DBLCDTempBlack"] )
//		self.exampleTextLabel.text = [NSString stringWithFormat:@"%@:%@",exampleText,size];
//	
    
//    LoggingOnlyComposeString logger = ^ NSString * (void)
//    {
//        return [NSString stringWithFormat:@"Size is %f", size];
//    };
//    
//	DNSLog(@"%@", logger());
}

- (NSAttributedString *)attrStringForString:(NSString *)string withColor:(UIColor *)color
{
    NSDictionary * attributes = @{NSForegroundColorAttributeName:color};
    NSAttributedString * attrString = [[NSAttributedString alloc] initWithString:string attributes:attributes];
    return attrString;
}

- (id) debugQuickLookObject
{
    NSMutableAttributedString * finalString = [[NSMutableAttributedString alloc] initWithString:@""];
    [finalString appendAttributedString:[self attrStringForString:@"font info:" withColor:[UIColor blackColor]]];
    [finalString appendAttributedString:[self attrStringForString:[self.exampleTextLabel.font description] withColor:[UIColor blueColor]]];
    
    [finalString appendAttributedString:[self attrStringForString:@" Text:" withColor:[UIColor blackColor]]];
    [finalString appendAttributedString:[self attrStringForString:self.exampleTextLabel.text withColor:[UIColor redColor]]];

    return finalString;
}

@end

